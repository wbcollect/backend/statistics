package com.wbcollect.statistics.initializer;

import com.google.protobuf.Timestamp;
import com.wbcollect.worker.WorkerGrpc;
import com.wbcollect.worker.WorkerProto;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

public class WorkerGrpcServerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private final Server SERVER = ServerBuilder.forPort(0)
            .addService(getBindableService())
            .build();

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        try {
            SERVER.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        TestPropertyValues.of(
                "com.wbcollect.common.service-urls.worker=" + "localhost:" + SERVER.getPort()
        ).applyTo(applicationContext);
    }

    protected WorkerGrpc.WorkerImplBase getBindableService() {
        return new WorkerGrpc.WorkerImplBase() {
            @Override
            public void getProductDataTs(WorkerProto.ProductDataTsRequest request, StreamObserver<WorkerProto.ProductDataTsReply> responseObserver) {
                var instantTime1 = Instant.parse("2023-05-07T00:00:00.00Z");
                var instantTime2 = instantTime1.plus(Duration.ofDays(1));
                var instantTime3 = instantTime1.plus(Duration.ofDays(2));
                var instantTime4 = instantTime1.plus(Duration.ofDays(3));
                var instantTime5 = instantTime1.plus(Duration.ofDays(4));
                var instantTime6 = instantTime1.plus(Duration.ofDays(5));
                var instantTime7 = instantTime1.plus(Duration.ofDays(6));
                responseObserver.onNext(
                        WorkerProto.ProductDataTsReply
                                .newBuilder()
                                .addAllProductDataTsArray(List.of(
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(10)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime1.getEpochSecond())
                                                        .setNanos(instantTime1.getNano())
                                                )
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy")
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(124L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(10)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime1.getEpochSecond())
                                                        .setNanos(instantTime1.getNano())
                                                )
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/tetradi")
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(21)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime2.getEpochSecond())
                                                        .setNanos(instantTime2.getNano())
                                                )
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(20)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy")
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime3.getEpochSecond())
                                                        .setNanos(instantTime3.getNano())
                                                )
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(200)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy")
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime4.getEpochSecond())
                                                        .setNanos(instantTime4.getNano())
                                                )
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(70)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy")
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime5.getEpochSecond())
                                                        .setNanos(instantTime5.getNano())
                                                )
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(77)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy")
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime6.getEpochSecond())
                                                        .setNanos(instantTime6.getNano())
                                                )
                                                .build(),
                                        WorkerProto.ProductDataTs.newBuilder()
                                                .setProductId(123L)
                                                .setBrand("adidas")
                                                .setSeller("Wildberries")
                                                .setQuantity(21)
                                                .setPrice(100)
                                                .setPriceWithSale(100)
                                                .setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy")
                                                .setTakenAt(Timestamp.newBuilder()
                                                        .setSeconds(instantTime7.getEpochSecond())
                                                        .setNanos(instantTime7.getNano())
                                                )
                                                .build()
                                ))
                                .build()
                );
                responseObserver.onCompleted();
            }
        };
    }
}
