package com.wbcollect.statistics;

import com.wbcollect.initializers.PostgresInitializer;
import com.wbcollect.statistics.grpc.StatisticsGrpcService;
import com.wbcollect.statistics.initializer.WorkerGrpcServerInitializer;
import com.wbcollect.statistics.scheduler.ScheduledInfoReceiver;
import io.grpc.internal.testing.StreamRecorder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Testcontainers
@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        PostgresInitializer.class,
        WorkerGrpcServerInitializer.class,
})
@EnableAutoConfiguration
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class StatisticsApplicationTests {
    @Container
    private static final PostgreSQLContainer POSTGRE_SQL_CONTAINER = PostgresInitializer.POSTGRE_SQL_CONTAINER;

    @Autowired
    private StatisticsGrpcService statisticsGrpcService;

    @Autowired
    private ScheduledInfoReceiver scheduledInfoReceiver;

    @Test
    @Transactional
    void preparesRequestedDailyInfoFromWorker_sendsProductInfoToInbound() throws Exception {
        var instant = Instant.parse("2023-05-07T00:00:00.00Z").plus(Duration.ofDays(6));
        // worker -> statistics
        scheduledInfoReceiver.takeInfoFromWorkerAndUpdateDatabase();

        // statistics -> inbound
        var requestFromStatisticService = StatisticsProto.ProductInfoRequest.newBuilder().setId(123L).build();
        StreamRecorder<StatisticsProto.ProductInfoReply> productInfoResponseObserver = StreamRecorder.create();
        statisticsGrpcService.getProductInfo(requestFromStatisticService, productInfoResponseObserver);

        if (!productInfoResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }

        Assertions.assertNull(productInfoResponseObserver.getError());

        List<StatisticsProto.ProductInfoReply> results = productInfoResponseObserver.getValues();
        Assertions.assertEquals(1, results.size());
        var response = results.get(0).getProductInfo();

        Assertions.assertEquals(123, response.getId());
        Assertions.assertEquals(100, response.getPrice());
        Assertions.assertEquals(instant.getEpochSecond(), response.getDate().getSeconds());
        Assertions.assertEquals(instant.getNano(), response.getDate().getNanos());
        Assertions.assertEquals("adidas", response.getBrandName());
    }

    @Test
    @Transactional
    void preparesRequestedDailyInfoFromWorker_sendsProductStatsToInbound() throws Exception {
        var instant = Instant.parse("2023-05-07T00:00:00.00Z");
        // worker -> statistics
        scheduledInfoReceiver.takeInfoFromWorkerAndUpdateDatabase();

        // statistics -> inbound
        var requestFromStatisticService = StatisticsProto.ProductStatsRequest.newBuilder().setId(123L).setNumberOfDays(7).build();
        StreamRecorder<StatisticsProto.ProductStatsReply> productStatsResponseObserver = StreamRecorder.create();
        statisticsGrpcService.getProductStats(requestFromStatisticService, productStatsResponseObserver);

        if (!productStatsResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }

        Assertions.assertNull(productStatsResponseObserver.getError());

        List<StatisticsProto.ProductStatsReply> results = productStatsResponseObserver.getValues();
        Assertions.assertEquals(1, results.size());
        var responseList = results.get(0).getProductStatsList();

        Assertions.assertEquals(7, responseList.size());

        var productStat = responseList.get(0);

        Assertions.assertEquals(10, productStat.getQuantity());
        Assertions.assertEquals(100, productStat.getPrice());
        Assertions.assertEquals(instant.getEpochSecond(), productStat.getDate().getSeconds());
        Assertions.assertEquals(instant.getNano(), productStat.getDate().getNanos());
        Assertions.assertEquals(0, productStat.getSales());
        Assertions.assertEquals(100, productStat.getRatingPercentage());
    }

    @Test
    @Transactional
    void preparesRequestedDailyInfoFromWorker_sendsCategoryStatsToInbound() throws Exception {
        var instant = Instant.parse("2023-05-07T00:00:00.00Z")
                .plus(Duration.ofDays(4));
        // worker -> statistics
        scheduledInfoReceiver.takeInfoFromWorkerAndUpdateDatabase();

        // statistics -> inbound
        var requestFromStatisticService = StatisticsProto.CategoryStatsRequest.newBuilder().setCategoryUrl("/knigi-i-kantstovary/kantstovary/karty-i-globusy").setNumberOfDays(365).build();
        StreamRecorder<StatisticsProto.CategoryStatsReply> categoryStatsResponseObserver = StreamRecorder.create();
        statisticsGrpcService.getCategoryStats(requestFromStatisticService, categoryStatsResponseObserver);

        if (!categoryStatsResponseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }

        Assertions.assertNull(categoryStatsResponseObserver.getError());

        List<StatisticsProto.CategoryStatsReply> results = categoryStatsResponseObserver.getValues();
        Assertions.assertEquals(1, results.size());
        var responseList = results.get(0).getCategoryStatsList();

        Assertions.assertEquals(6, responseList.size());

        var categoryStats = responseList.get(3);

        Assertions.assertEquals(130, categoryStats.getSales());
        Assertions.assertEquals(13000, categoryStats.getRevenue());
        Assertions.assertEquals(instant.getEpochSecond(), categoryStats.getDate().getSeconds());
        Assertions.assertEquals(instant.getNano(), categoryStats.getDate().getNanos());
    }

}
