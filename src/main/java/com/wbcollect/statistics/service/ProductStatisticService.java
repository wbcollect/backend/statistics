package com.wbcollect.statistics.service;

import com.wbcollect.statistics.StatisticsProto;
import com.wbcollect.statistics.repository.ProductInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static com.wbcollect.statistics.util.Utils.mapProductInfoToProductInfoGrpc;
import static com.wbcollect.statistics.util.Utils.mapProductInfoToProductStats;

@Service
@RequiredArgsConstructor
public class ProductStatisticService {
    private final ProductInfoRepository productInfoRepository;

    public StatisticsProto.ProductStatsReply retrieveProductStats(long id, int numberOfDays) {
        var productsArray = productInfoRepository.findAllByProductIdOrderByTakenAtDesc(id, PageRequest.of(0, numberOfDays));

        List<StatisticsProto.ProductStats> resultArray = new ArrayList<>();
        productsArray.forEach(productInfo -> resultArray.add(mapProductInfoToProductStats(productInfo)));
        Collections.reverse(resultArray);
        return StatisticsProto.ProductStatsReply
                .newBuilder()
                .setStatus(200)
                .addAllProductStats(resultArray)
                .build();
    }

    public StatisticsProto.ProductInfoReply retrieveProductInfo(long id) {
        var optionalProductInfo = productInfoRepository.findFirstByProductIdOrderByTakenAtDesc(id);
        if (optionalProductInfo.isEmpty()) {
            throw new NoSuchElementException("Product info not found");
        }
        var productInfo = optionalProductInfo.get();
        return StatisticsProto.ProductInfoReply.newBuilder()
                .setProductInfo(mapProductInfoToProductInfoGrpc(productInfo))
                .setStatus(200)
                .build();
    }
}
