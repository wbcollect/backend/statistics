package com.wbcollect.statistics.service;

import com.wbcollect.statistics.StatisticsProto;
import com.wbcollect.statistics.entity.ProductInfo;
import com.wbcollect.statistics.repository.ProductInfoRepository;
import io.grpc.Status;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.wbcollect.statistics.util.Utils.*;

@Service
@RequiredArgsConstructor
public class CategoryStatisticsService {
    private final ProductInfoRepository productInfoRepository;


    public StatisticsProto.CategoryStatsReply retrieveCategoryStats(String categoryUrl, int numberOfDays) {
        var productsArray = getAllMatchingProductsFromDatabase(categoryUrl, numberOfDays);
        productsArray.forEach(productInfo -> productInfo.setTakenAt(truncateDateToDay(productInfo.getTakenAt())));
        var resultArray = productsArray.stream()
                .collect(Collectors.groupingBy(ProductInfo::getTakenAt))
                .values()
                .stream()
                .map(list -> {
                    if (list.size() == 0)
                        return StatisticsProto.CategoryStats.getDefaultInstance();
                    int sales = 0;
                    int revenue = 0;
                    Date date = null;

                    for (ProductInfo product : list) {
                        if (date == null) {
                            date = product.getTakenAt();
                        }
                        sales += product.getSales();
                        revenue += product.getSales() * product.getCurrentPrice();
                    }

                    return StatisticsProto.CategoryStats.newBuilder()
                            .setDate(mapDateToTimestamp(date))
                            .setRevenue(revenue)
                            .setSales(sales)
                            .build();
                })
                .sorted(Comparator.comparingLong(stats -> stats.getDate().getSeconds()))
                .collect(Collectors.toList());
        return StatisticsProto.CategoryStatsReply
                .newBuilder()
                .addAllCategoryStats(resultArray)
                .build();
    }

    private List<ProductInfo> getAllMatchingProductsFromDatabase(String categoryUrl, int numberOfDays) {
        var splitUrl = categoryUrl.split("/+");
        String category0 = splitUrl[1];
        String category1 = (splitUrl.length > 2) ? splitUrl[2] : null;
        String category2 = (splitUrl.length > 3) ? splitUrl[3] : null;
        String category3 = (splitUrl.length > 4) ? splitUrl[4] : null;
        Date dateThreshold = truncateDateToDay(Date.from(Instant.now().minus(Duration.ofDays(numberOfDays))));

        if (category3 != null) {
            return productInfoRepository.findAllByCategory0AndCategory1AndCategory2AndCategory3AndTakenAtAfterOrderByTakenAtDesc(
                    category0, category1, category2, category3, dateThreshold);
        }
        if (category2 != null) {
            return productInfoRepository.findAllByCategory0AndCategory1AndCategory2AndTakenAtAfterOrderByTakenAtDesc(
                    category0, category1, category2, dateThreshold);
        }
        if (category1 != null) {
            return productInfoRepository.findAllByCategory0AndCategory1AndTakenAtAfterOrderByTakenAtDesc(
                    category0, category1, dateThreshold);
        }
        return productInfoRepository.findAllByCategory0AndTakenAtAfterOrderByTakenAtDesc(
                category0, dateThreshold);
    }

}
