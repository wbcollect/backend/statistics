package com.wbcollect.statistics.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
@IdClass(ProductInfoId.class)
@Table(name = "product_info")
public class ProductInfo {
    @Id
    @Column(name = "product_id", nullable = false)
    private Long productId;

    @Id
    @Column(name = "taken_at", nullable = false)
    private Date takenAt;

    @Column(name = "product_name")
    private String productName;

    @Column
    private String seller;

    @Column
    private String brand;

    @Column
    private int quantity;

    @Column
    private int sales;

    @Column(name = "current_price")
    private int currentPrice;

    @Column
    private String category0;

    @Column
    private String category1;

    @Column
    private String category2;

    @Column
    private String category3;
}
