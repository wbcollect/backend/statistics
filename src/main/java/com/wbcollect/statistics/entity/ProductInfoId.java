package com.wbcollect.statistics.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfoId implements Serializable {
    private Long productId;

    private Date takenAt;
}
