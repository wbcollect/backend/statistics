package com.wbcollect.statistics.grpc;

import com.wbcollect.statistics.StatisticsGrpc;
import com.wbcollect.statistics.StatisticsProto;
import com.wbcollect.statistics.service.CategoryStatisticsService;
import com.wbcollect.statistics.service.ProductStatisticService;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@GrpcService
@RequiredArgsConstructor
public class StatisticsGrpcService extends StatisticsGrpc.StatisticsImplBase {
    private final ProductStatisticService productStatisticService;
    private final CategoryStatisticsService categoryStatisticsService;

    @Override
    public void getProductStats(StatisticsProto.ProductStatsRequest request, StreamObserver<StatisticsProto.ProductStatsReply> responseObserver) {
        log.info("Received request from inbound gateway to send statistics about product with id: " +
                request.getId() + " for the last " + request.getNumberOfDays() + " days.");
        try {
            responseObserver.onNext(productStatisticService.retrieveProductStats(request.getId(), request.getNumberOfDays()));
            responseObserver.onCompleted();
        } catch (Exception e) {
            log.error("Exception occurred during getting statistics for product with id: " + request.getId(), e);
            responseObserver.onError(Status.INTERNAL.withDescription("Internal server error").withCause(e).asRuntimeException());
        }
    }

    @Override
    public void getProductInfo(StatisticsProto.ProductInfoRequest request, StreamObserver<StatisticsProto.ProductInfoReply> responseObserver) {
        log.info("Received request from inbound gateway to send info about product with id: " + request.getId());
        try {
            responseObserver.onNext(productStatisticService.retrieveProductInfo(request.getId()));
            responseObserver.onCompleted();
        } catch (NoSuchElementException e) {
            log.error("Product info not found for product with id: " + request.getId(), e);
            responseObserver.onError(Status.NOT_FOUND.withDescription("Product info not found").withCause(e).asRuntimeException());
        } catch (Exception e) {
            log.error("Exception occurred during getting info for product with id: " + request.getId(), e);
            responseObserver.onError(Status.INTERNAL.withDescription("Internal server error").withCause(e).asRuntimeException());
        }
    }

    @Override
    public void getCategoryStats(StatisticsProto.CategoryStatsRequest request, StreamObserver<StatisticsProto.CategoryStatsReply> responseObserver) {
        log.info("Received request from inbound gateway to send statistics about category with url: " +
                request.getCategoryUrl() + " for the last " + request.getNumberOfDays() + " days.");
        try {
            String regex = "^/([a-z-]+)/?([a-z-]*)/?([a-z-]*)/?([a-z-]*)$";

            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(request.getCategoryUrl());
            if (!matcher.matches()) {
                responseObserver.onError(Status.INVALID_ARGUMENT
                        .withDescription("Category URL should match pattern: /category0/category1/category2/category3")
                        .asRuntimeException()
                );
                return;
            }
            var result = categoryStatisticsService.retrieveCategoryStats(request.getCategoryUrl(), request.getNumberOfDays());
            if (result.getCategoryStatsList().size() == 0) {
                responseObserver.onError(Status.NOT_FOUND
                        .withDescription("No info about provided category")
                        .asRuntimeException()
                );
                return;
            }
            responseObserver.onNext(result);
            responseObserver.onCompleted();
        } catch (Exception e) {
            log.error("Exception occurred during getting statistics for category with url: " + request.getCategoryUrl(), e);
            responseObserver.onError(Status.INTERNAL.withDescription("Internal server error").withCause(e).asRuntimeException());
        }
    }

}
