package com.wbcollect.statistics.util;

import com.google.protobuf.Timestamp;
import com.wbcollect.statistics.StatisticsProto;
import com.wbcollect.statistics.entity.ProductInfo;
import com.wbcollect.worker.WorkerProto;
import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@UtilityClass
public class Utils {
    public static StatisticsProto.ProductStats mapProductInfoToProductStats(ProductInfo productInfo) {
        return StatisticsProto.ProductStats.newBuilder()
                .setDate(mapDateToTimestamp(productInfo.getTakenAt()))
                .setPrice(productInfo.getCurrentPrice())
                .setQuantity(productInfo.getQuantity())
                // TODO: add rating
                .setRatingPercentage(100)
                .setSales(productInfo.getSales())
                .build();
    }

    public static StatisticsProto.ProductInfo mapProductInfoToProductInfoGrpc(ProductInfo productInfo) {
        return StatisticsProto.ProductInfo.newBuilder()
                .setDate(mapDateToTimestamp(productInfo.getTakenAt()))
                .setId(productInfo.getProductId())
                .setName(productInfo.getProductName())
                .setPrice(productInfo.getCurrentPrice())
                .setPriceWithSale(productInfo.getCurrentPrice())
                .setBrandName(productInfo.getBrand())
                .build();
    }

    public static ProductInfo mapProductDataTsToProductInfo(WorkerProto.ProductDataTs productDataTs, int sales) {
        Date takenAt = Date.from(Instant.ofEpochSecond(
                productDataTs.getTakenAt().getSeconds(),
                productDataTs.getTakenAt().getNanos())
        );
        String[] splitUrl = productDataTs.getCategoryUrl().split("/+");

        return ProductInfo.builder()
                .productId(productDataTs.getProductId())
                .takenAt(takenAt)
                .productName(productDataTs.getName())
                .currentPrice(productDataTs.getPriceWithSale())
                .seller(productDataTs.getSeller())
                .brand(productDataTs.getBrand())
                .category0(takeCategory(splitUrl, 0))
                .category1(takeCategory(splitUrl, 1))
                .category2(takeCategory(splitUrl, 2))
                .category3(takeCategory(splitUrl, 3))
                .quantity(productDataTs.getQuantity())
                .sales(sales)
                .build();
    }

    public static Timestamp mapDateToTimestamp(Date date) {
        var dateInstant = date.toInstant();
        return Timestamp.newBuilder()
                .setSeconds(dateInstant.getEpochSecond())
                .setNanos(dateInstant.getNano())
                .build();
    }

    public static Date truncateDateToDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return truncateCalendarToDay(cal);
    }

    public static Date truncateDateToWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, 0);
        return truncateCalendarToDay(cal);
    }

    public static Date truncateDateToMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.WEEK_OF_MONTH, 0);
        return truncateCalendarToDay(cal);
    }

    private static Date truncateCalendarToDay(Calendar cal) {
        cal.setTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    // splited array has empty str in 0 cell, so pos + 1
    public static String takeCategory(String[] splitUrl, int position) {
        if (position + 1 < splitUrl.length)
            return splitUrl[position + 1];
        return "";
    }
}
