package com.wbcollect.statistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class StatisticsApplication {

    /**
     * Task by cron 00:10 min goes to worker and takes data for prior day
     * - sales for product
     * - revenue per product
     * - ^^^^ per day, per week (MON-SUN), per month (01-31)
     * - ^^^^^^^^ per category
     *
     * group by grouping sets ((product), (category), (subcategory), (day), (category, day|week|month))
     *
     * table:
     * PRODUCTID, DATE, CATEGORY, SUBCATEGORY, SELLER, BRAND, ОСТАТОК, ПРОДАЖИ, PRICE
     *
     * */

    public static void main(String[] args) {
        SpringApplication.run(StatisticsApplication.class, args);
    }

}
