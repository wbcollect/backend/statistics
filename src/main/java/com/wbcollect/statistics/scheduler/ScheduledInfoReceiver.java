package com.wbcollect.statistics.scheduler;

import com.google.protobuf.Timestamp;
import com.wbcollect.statistics.entity.ProductInfo;
import com.wbcollect.statistics.repository.ProductInfoRepository;
import com.wbcollect.worker.WorkerGrpc;
import com.wbcollect.worker.WorkerProto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import static com.wbcollect.statistics.util.Utils.mapProductDataTsToProductInfo;

@Slf4j
@Component
@RequiredArgsConstructor
public class ScheduledInfoReceiver {
    private final WorkerGrpc.WorkerBlockingStub workerBlockingStub;

    private final ProductInfoRepository productInfoRepository;

    /**
     * Every day at 0:30 goes to worker for info
     */

    @Scheduled(cron = "0 30 0 * * *")
    public void takeInfoFromWorkerAndUpdateDatabase() {

        List<WorkerProto.ProductDataTs> productDataTsList = getProductDataTsList();

        log.info("List with product info received, size: " + productDataTsList.size());

        updateDatabase(productDataTsList);

    }

    private List<WorkerProto.ProductDataTs> getProductDataTsList() {
        var endTime = Instant.now(); // current date
        var startTime = endTime.minus(Duration.ofDays(1));
        var productDataTsListRequest = WorkerProto.ProductDataTsRequest
                .newBuilder()
                .setStartDate(Timestamp.newBuilder()
                        .setSeconds(startTime.getEpochSecond())
                        .setNanos(startTime.getNano())
                        .build()
                )
                .setEndDate(Timestamp.newBuilder()
                        .setSeconds(endTime.getEpochSecond())
                        .setNanos(endTime.getNano())
                        .build()
                )
                .build();

        return workerBlockingStub
                .getProductDataTs(productDataTsListRequest)
                .getProductDataTsArrayList();
    }

    private void updateDatabase(List<WorkerProto.ProductDataTs> productDataTsList) {
        for (WorkerProto.ProductDataTs productDataTs : productDataTsList) {
            var prevProductInfo = productInfoRepository
                    .findFirstByProductIdOrderByTakenAtDesc(productDataTs.getProductId());
            int sales = 0;
            if (prevProductInfo.isPresent() && prevProductInfo.get().getQuantity() > productDataTs.getQuantity()) {
                sales = prevProductInfo.get().getQuantity() - productDataTs.getQuantity();
            }
            ProductInfo newProductInfo = mapProductDataTsToProductInfo(productDataTs, sales);
            productInfoRepository.save(newProductInfo);
        }
    }
}
