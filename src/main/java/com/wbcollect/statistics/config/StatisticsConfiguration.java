package com.wbcollect.statistics.config;

import com.wbcollect.common.GrpcAutoConfiguration;
import com.wbcollect.common.grpc.StubFactory;
import com.wbcollect.worker.WorkerGrpc;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ImportAutoConfiguration({
        GrpcAutoConfiguration.class
})
@RequiredArgsConstructor
public class StatisticsConfiguration {
    private final ConfigClientProperties configClientProperties;

    @Bean
    public ManagedChannel workerChannel() {
        return NettyChannelBuilder.forTarget(resolveServiceName("worker"))
                .usePlaintext()
                .defaultLoadBalancingPolicy("round_robin")
                .build();
    }

    private String resolveServiceName(String serviceName) {
        return configClientProperties.getServiceUrls().computeIfAbsent(serviceName, key -> {
            throw new BeanInitializationException(
                    "Unable to resolve service URL for '" + key + "'. " +
                            "Consider to add " +
                            "'com.wbcollect.common.service-urls.worker=${WORKER_HOST:localhost}:${WORKER_GRPC_PORT:9090}' into your application.properties(.yaml) file.");
        });
    }

    @Bean
    public WorkerGrpc.WorkerBlockingStub workerBlockingStub(ManagedChannel managedChannel, StubFactory stubFactory) {
        return stubFactory.deadlinedStub(
                WorkerGrpc.WorkerBlockingStub.class,
                managedChannel);
    }
}
