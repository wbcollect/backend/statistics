package com.wbcollect.statistics.repository;

import com.wbcollect.statistics.entity.ProductInfo;
import com.wbcollect.statistics.entity.ProductInfoId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ProductInfoRepository extends JpaRepository<ProductInfo, ProductInfoId> {
    Optional<ProductInfo> findFirstByProductIdOrderByTakenAtDesc(Long productId);

    List<ProductInfo> findAllByProductIdOrderByTakenAtDesc(Long productId, Pageable pageable);

    List<ProductInfo> findAllByCategory0AndCategory1AndCategory2AndCategory3AndTakenAtAfterOrderByTakenAtDesc(
            String category0, String category1, String category2, String category3, Date startDate);
    List<ProductInfo> findAllByCategory0AndCategory1AndCategory2AndTakenAtAfterOrderByTakenAtDesc(
            String category0, String category1, String category2, Date startDate);
    List<ProductInfo> findAllByCategory0AndCategory1AndTakenAtAfterOrderByTakenAtDesc(
            String category0, String category1, Date startDate);
    List<ProductInfo> findAllByCategory0AndTakenAtAfterOrderByTakenAtDesc(
            String category0, Date startDate);
}
