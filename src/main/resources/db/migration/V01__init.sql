-- First migration script for ProductInfo table
CREATE TABLE IF NOT EXISTS product_info
(
    product_id BIGINT NOT NULL,
    taken_at TIMESTAMP(6) NOT NULL,
    seller VARCHAR(255),
    brand VARCHAR(255),
    quantity INTEGER,
    sales INTEGER,
    current_price INTEGER,
    category0 VARCHAR(255),
    category1 VARCHAR(255),
    category2 VARCHAR(255),
    category3 VARCHAR(255),
    PRIMARY KEY (product_id, taken_at)
);