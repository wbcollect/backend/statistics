# Микросервис Statistics

Микросервис Statistics отвечает за ежедневный сбор статистики от сервиса Worker и её агрегацию для более быстрой выдачи клиентам.

## Используемые технологии

- Spring Boot
- Spring Data JPA
- gRPC
- PostgreSQL + Flyway
